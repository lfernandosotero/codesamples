// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CPPExample_HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDamageTaken);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CODESAMPLES_API UCPPExample_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCPPExample_HealthComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Health)
	float CurrentHealth;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Health)
	float MaxHealth;

	//Owner current health regen in Amount per second
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Health)
	float HealthRegen;

	UPROPERTY(BlueprintReadOnly, BlueprintReadOnly, Category = Health)
	bool bIsDead;

protected:
	virtual void BeginPlay() override;

	void RegenHP(float deltaTime);
	void Die();

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void TakeDamage(float DamageAmount);

	//Delegates
	FOnDamageTaken OnDamageTakenDelegate;
	FOnDeath OnDeathDelegate;
};
