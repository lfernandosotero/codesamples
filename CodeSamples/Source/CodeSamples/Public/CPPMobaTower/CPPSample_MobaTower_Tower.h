// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPPSample_MobaTower_Tower.generated.h"

class UStaticMeshComponent;
class USphereComponent;
class ACPPSample_MobaTower_TowerShot;

UCLASS()
class CODESAMPLES_API ACPPSample_MobaTower_Tower : public AActor
{
	GENERATED_BODY()
	
public:	
	ACPPSample_MobaTower_Tower();

	/*Time in seconds between tower shots
		tower can only shoot when it has spent
		TimeBetweenAttacks seconds without shooting
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Shooting)
	float TimeBetweenAttacks;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Shooting)
	TSubclassOf<ACPPSample_MobaTower_TowerShot> Projectile;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	USceneComponent* MySceneRoot;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Shooting)
	UStaticMeshComponent* ShooterMesh;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Shooting)
	USphereComponent* RangeCollider;

	AActor* CurrentTarget;
	float BiggestAggroInRange;
	//Time since last shot
	float CurrentAttackTimer;

	void DeterminateBestAggro();
	void ResetTarget();
	void SetNewTarget(AActor* NewTarget);
	void DrawAimLine();
	void AttackTarget(float DeltaTime);

	void CheckIfTargetValid();
	void CheckPlayerAggros(TArray<AActor*> OverlappingCharacters);
	void CheckMinionAggros(TArray<AActor*> OverlappingMinions);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void HandleEndOverlap(AActor* OverlappedActor, AActor* OtherActor);
};
