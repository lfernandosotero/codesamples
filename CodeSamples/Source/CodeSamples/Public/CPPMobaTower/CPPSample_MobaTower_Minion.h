// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CPPSample_MobaTower_Minion.generated.h"

class UCPPSample_AggroComponent;
class UCPPExample_HealthComponent;

UCLASS()
class CODESAMPLES_API ACPPSample_MobaTower_Minion : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACPPSample_MobaTower_Minion();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//My Mesh representation
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UStaticMeshComponent* CylinderComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UCPPSample_AggroComponent* MyAggroComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UCPPExample_HealthComponent* MyHealthComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void TakenAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
	void OnDeathDelegate();
};
