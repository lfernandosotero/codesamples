// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CPPSample_AggroComponent.generated.h"


/* Aggro Component calculates an Aggro for a specific actor based on his status and health
	Aggro is a value that informs how high priority this actor is for other actors to target or interact with
	Actors generate Aggro when attacking enemy champions and have higher values when on low HP
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CODESAMPLES_API UCPPSample_AggroComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCPPSample_AggroComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Aggro)
	float CurrentAggro;

	/*Ratio on how the aggro increases by missing health
	Any value for Current Health between 0 and Max Health gives an Aggro in [0 - Value]
	A value of 10 theoretically means that when the Current health / Max Health is 0 the actor gains 10 Aggro
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Aggro)
	float AggroIncreaseByHealth; 
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Aggro)
	bool bIsPriority;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Aggro)
	float PriorityAggro;

	/* Base Aggro values for Players and Minions*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default Aggro values")
	float MinionBaseAggro;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default Aggro values")
	float PlayerBaseAggro;

	/* Colors used for debugging*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default Aggro values")
	FLinearColor LowAggroColor;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default Aggro values")
	FLinearColor HighAggroColor;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void CalculateAggro();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
