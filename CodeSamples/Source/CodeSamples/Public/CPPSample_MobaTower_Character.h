// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CPPSample_MobaTower_Character.generated.h"

class UCPPExample_HealthComponent;
class UCPPSample_AggroComponent;

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class CODESAMPLES_API ACPPSample_MobaTower_Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACPPSample_MobaTower_Character();
	bool bIsDead;

protected:
	virtual void BeginPlay() override;
	void MoveForward(float Scale);
	void MoveRight(float Scale);
	void SimulateAttackingEnemy();
	void StopSimulatingAttackingEnemy();

	//Camera Components
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera)
	UCameraComponent* CameraComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera)
	USpringArmComponent* CameraBoom;

	//My Moba-Related Components
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UCPPExample_HealthComponent* MyHealthComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UCPPSample_AggroComponent* MyAggroComponent;

public:	
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void TakenAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
	void OnDeathDelegate();

};
