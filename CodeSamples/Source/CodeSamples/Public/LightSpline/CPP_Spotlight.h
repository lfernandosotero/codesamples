// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CPP_Spotlight.generated.h"

class AActor;
class UCPP_PickableComponent;
class UStaticMeshComponent;

UCLASS()
class CODESAMPLES_API ACPP_Spotlight : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_Spotlight();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	FVector LookAtTarget;

	//Components
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	UCPP_PickableComponent* MyPickableComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	UStaticMeshComponent* SpotlightMesh;

	//Functions
	void SetLookAtTarget();
	void RotateTowardsTarget(float DeltaTime);
	void DrawDebugTargetLocation();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	AActor* TargetActor;
	float LookAtSpeed;
	bool bIsDebugOn;
	bool bIsAttached;
	bool bStartSimulatingPhysics;

	bool bTrackPlayerSight;
	bool bTrackCharacter;
	bool bTrackLastSeenPosition;
};
