// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CPP_PickableComponent.generated.h"

class UArrowComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CODESAMPLES_API UCPP_PickableComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCPP_PickableComponent();

	bool bCanBeGrabbed;
	bool bStartSimulatingPhysics;

protected:
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	void SetupComponent(bool bCanBeGrabbed, bool bStartSimulatingPhysics);
	void DropMeDown();
};
