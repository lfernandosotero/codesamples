// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPSample_AggroComponent.h"
#include "Engine.h"
#include "CPPExample_HealthComponent.h"

// Sets default values for this component's properties
UCPPSample_AggroComponent::UCPPSample_AggroComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	LowAggroColor = FLinearColor::Green;
	HighAggroColor = FLinearColor::Red;

	PriorityAggro = 100;
	MinionBaseAggro = 50;
	PlayerBaseAggro = 10;
	AggroIncreaseByHealth = 10;
}


// Called when the game starts
void UCPPSample_AggroComponent::BeginPlay()
{
	Super::BeginPlay();
}

/* Calculates Aggro based on the Owner Status and current health
	Aggro increases based on how low the HP of the Owner is
	If the Owner is dead, Aggro is 0
	If Owner is attacking enemies, he becomes a priority target. Aggro is 'PriorityAggro'
*/
void UCPPSample_AggroComponent::CalculateAggro()
{
	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner) {
		UCPPExample_HealthComponent* OwnerHealthComponent = ComponentOwner->FindComponentByClass<UCPPExample_HealthComponent>();
		if (OwnerHealthComponent) {
			if (OwnerHealthComponent->bIsDead) {
				CurrentAggro = 0;
			}
			else {
				if (bIsPriority) {
					CurrentAggro = PriorityAggro;
				}
				else {
					float HealthRatio = 1.0f - (OwnerHealthComponent->CurrentHealth / OwnerHealthComponent->MaxHealth);
					float AggroBasedOnHealth = HealthRatio * AggroIncreaseByHealth;

					//Apply base aggros based on the class of the Owner
					if (Cast<ACharacter>(ComponentOwner)) {
						CurrentAggro = PlayerBaseAggro + AggroBasedOnHealth;
					}
					else {
						CurrentAggro = MinionBaseAggro + AggroBasedOnHealth;
					}
				}
			}
		}
	}
	
}


//Update Owner's Aggro on each frame
void UCPPSample_AggroComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	CalculateAggro();
}

