// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPSample_MobaTower_Minion.h"
#include "Components/StaticMeshComponent.h"
#include "CPPSample_AggroComponent.h"
#include "CPPExample_HealthComponent.h"

// Sets default values
ACPPSample_MobaTower_Minion::ACPPSample_MobaTower_Minion()
{
	PrimaryActorTick.bCanEverTick = true;
	CylinderComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Minion Representation"));
	CylinderComponent->SetupAttachment(RootComponent);


	MyHealthComponent = CreateDefaultSubobject<UCPPExample_HealthComponent>(TEXT("Health Component"));
	MyAggroComponent = CreateDefaultSubobject<UCPPSample_AggroComponent>(TEXT("Aggro Component"));

	AddOwnedComponent(MyHealthComponent);
	AddOwnedComponent(MyAggroComponent);

	MyHealthComponent->OnDeathDelegate.AddDynamic(this, &ACPPSample_MobaTower_Minion::OnDeathDelegate);
	OnTakeAnyDamage.AddDynamic(this, &ACPPSample_MobaTower_Minion::TakenAnyDamage);
}

// Called when the game starts or when spawned
void ACPPSample_MobaTower_Minion::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPPSample_MobaTower_Minion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACPPSample_MobaTower_Minion::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ACPPSample_MobaTower_Minion::TakenAnyDamage(AActor * DamagedActor, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	MyHealthComponent->TakeDamage(Damage);
}

void ACPPSample_MobaTower_Minion::OnDeathDelegate()
{
	Destroy();
}

