// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPExample_HealthComponent.h"

// Setting default health properties
UCPPExample_HealthComponent::UCPPExample_HealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bIsDead = false;
	MaxHealth = 100;
	HealthRegen = 0.1f;
	CurrentHealth = MaxHealth;
}

void UCPPExample_HealthComponent::BeginPlay()
{
	Super::BeginPlay();
}

//Receiving damage
void UCPPExample_HealthComponent::TakeDamage(float DamageAmount)
{
	float NewHP = CurrentHealth - DamageAmount;
	CurrentHealth = FMath::Max(0.0f, NewHP);
	OnDamageTakenDelegate.Broadcast();

	if (CurrentHealth == 0) {
		Die();
	}
}

//Applies base health regeneration
void UCPPExample_HealthComponent::RegenHP(float DeltaTime)
{
	float NewHP = CurrentHealth + HealthRegen * DeltaTime;
	if (!bIsDead) {
		CurrentHealth = FMath::Min(NewHP, MaxHealth);
	}
}

//Owner has died
void UCPPExample_HealthComponent::Die()
{
	bIsDead = true;
	OnDeathDelegate.Broadcast();
}


void UCPPExample_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	RegenHP(DeltaTime);

}

