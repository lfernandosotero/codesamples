// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPSample_MobaTower_TowerShot.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"
#include "Components/SceneComponent.h"

// Sets default values
ACPPSample_MobaTower_TowerShot::ACPPSample_MobaTower_TowerShot()
{
	PrimaryActorTick.bCanEverTick = true;

	//Default Scene Component
	MyRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Default Scene Component"));
	RootComponent = MyRootComponent;

	//Projectile Mesh with Overlap collision
	ProjectileMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	ProjectileMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ProjectileMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	ProjectileMeshComponent->SetUseCCD(true);
	ProjectileMeshComponent->SetupAttachment(RootComponent);
	
	//Projectile Movement Component
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement Component"));
	ProjectileMovementComponent->InitialSpeed = 500.0f;
	ProjectileMovementComponent->MaxSpeed = 500.0f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->ProjectileGravityScale = 0.0f;
	ProjectileMovementComponent->bIsHomingProjectile = true;
	ProjectileMovementComponent->bAutoActivate = false;
	ProjectileMovementComponent->Velocity = FVector::ZeroVector;
	ProjectileMovementComponent->HomingAccelerationMagnitude = 100000.0f;
	AddOwnedComponent(ProjectileMovementComponent);

	//Default Values
	Damage = 30;
}

//Setting up collision response handler and Movement Component Homing target
void ACPPSample_MobaTower_TowerShot::BeginPlay()
{
	Super::BeginPlay();
	//ProjectileMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ACPPSample_MobaTower_TowerShot::OnProjectileBeginOverlap);
}

// Called every frame
void ACPPSample_MobaTower_TowerShot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACPPSample_MobaTower_TowerShot::NotifyActorBeginOverlap(AActor* OtherActor) {
	Super::NotifyActorBeginOverlap(OtherActor);

	if (OtherActor == Target) {
		UGameplayStatics::ApplyDamage(Target, Damage, nullptr, GetOwner(), UDamageType::StaticClass());
		Destroy();
	}
}

void ACPPSample_MobaTower_TowerShot::StartProjectile()
{
	if (Target && Cast<ACharacter>(Target)) {
		ProjectileMovementComponent->HomingTargetComponent = Target->GetRootComponent();
	}
	ProjectileMovementComponent->Activate(true);
}

//void ACPPSample_MobaTower_TowerShot::OnProjectileBeginOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
//{
//	
//}

