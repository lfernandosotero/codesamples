// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPSample_MobaTower_Tower.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "DrawDebugHelpers.h"
#include "CPPSample_MobaTower_Character.h"
#include "CPPSample_MobaTower_Minion.h"
#include "GameFramework/Actor.h"
#include "CPPSample_AggroComponent.h"
#include "CPPExample_HealthComponent.h"
#include "CPPSample_MobaTower_TowerShot.h"

// Sets default values
ACPPSample_MobaTower_Tower::ACPPSample_MobaTower_Tower()
{
	MySceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Default Scene Root"));
	RootComponent = MySceneRoot;
 	
	//Setting Up components
	PrimaryActorTick.bCanEverTick = true;
	ShooterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Shooter"));
	ShooterMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ShooterMesh->SetupAttachment(RootComponent);

	RangeCollider = CreateDefaultSubobject<USphereComponent>(TEXT("Range Collider"));
	RangeCollider->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	RangeCollider->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	RangeCollider->SetSphereRadius(512.0f);
	RangeCollider->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACPPSample_MobaTower_Tower::BeginPlay()
{
	Super::BeginPlay();
	
	//Default Timer for Attack
	CurrentAttackTimer = TimeBetweenAttacks;
	OnActorEndOverlap.AddDynamic(this, &ACPPSample_MobaTower_Tower::HandleEndOverlap);
}

//Determine the aggro based on the Characters and Minions present in my Range
void ACPPSample_MobaTower_Tower::DeterminateBestAggro()
{
	TArray<AActor*> OverlappingCharacters;
	GetOverlappingActors(OverlappingCharacters, ACPPSample_MobaTower_Character::StaticClass());

	TArray<AActor*> OverlappingMinions;
	GetOverlappingActors(OverlappingMinions, ACPPSample_MobaTower_Minion::StaticClass());

	ResetTarget();

	if (OverlappingCharacters.Num() >= 1) {
		CheckPlayerAggros(OverlappingCharacters);
	}
	
	if (OverlappingMinions.Num() >= 1) {
		CheckMinionAggros(OverlappingMinions);
	}

	// No players or minions within Range, clear my target values
	if (!(OverlappingMinions.Num() >= 1 || OverlappingCharacters.Num() >= 1)) {
		ResetTarget();
	}
}

void ACPPSample_MobaTower_Tower::ResetTarget()
{
	BiggestAggroInRange = -10.0f;
	CurrentTarget = nullptr;
}

void ACPPSample_MobaTower_Tower::SetNewTarget(AActor * NewTarget)
{
	CurrentTarget = NewTarget;
}

void ACPPSample_MobaTower_Tower::DrawAimLine()
{
	if (CurrentTarget) {
		DrawDebugLine(GetWorld(), ShooterMesh->GetComponentLocation(), CurrentTarget->GetActorLocation(), FColor::Red, false, -1.0f, (uint8)'\000', 2.0f);
	}
}

void ACPPSample_MobaTower_Tower::CheckIfTargetValid()
{
	if (CurrentTarget) {
		UCPPExample_HealthComponent* CurrentTargetHP = CurrentTarget->FindComponentByClass<UCPPExample_HealthComponent>();

		if (CurrentTargetHP && CurrentTargetHP->bIsDead) {
			ResetTarget();
		}
	}
}

//Checks Aggro values for all players
void ACPPSample_MobaTower_Tower::CheckPlayerAggros(TArray<AActor*> OverlappingCharacters)
{
	for (AActor* Actor : OverlappingCharacters) {
		ACPPSample_MobaTower_Character* CurrentPlayer = Cast<ACPPSample_MobaTower_Character>(Actor);
		if (CurrentPlayer) {
			UCPPExample_HealthComponent* ActorHP = CurrentPlayer->FindComponentByClass<UCPPExample_HealthComponent>();
			UCPPSample_AggroComponent* ActorAggro = CurrentPlayer->FindComponentByClass<UCPPSample_AggroComponent>();
			//If Actor is dead, Continue iterating over the other players
			if (ActorHP && ActorHP->bIsDead) {
				continue;
			}

			//Actor isn't Dead And I Already have a current target set
			if (CurrentTarget) {
				if (ActorAggro) {
					if (ActorAggro->CurrentAggro >= ActorAggro->PriorityAggro) {
						BiggestAggroInRange = ActorAggro->CurrentAggro;
						SetNewTarget(CurrentPlayer);
						break;
					}
				}
			}
			//Actor isn't Dead And I Have no targets
			else {
				if (ActorAggro) {
					BiggestAggroInRange = ActorAggro->CurrentAggro;
					SetNewTarget(CurrentPlayer);
					break;
				}
			}
		}
	}
}

void ACPPSample_MobaTower_Tower::CheckMinionAggros(TArray<AActor*> OverlappingMinions)
{
	for (AActor* Actor : OverlappingMinions) {
		ACPPSample_MobaTower_Minion* CurrentMinion = Cast<ACPPSample_MobaTower_Minion>(Actor);
		if (CurrentMinion) {

			UCPPExample_HealthComponent* ActorHP = CurrentMinion->FindComponentByClass<UCPPExample_HealthComponent>();
			UCPPSample_AggroComponent* ActorAggro = CurrentMinion->FindComponentByClass<UCPPSample_AggroComponent>();

			//If Actor is dead, Continue iterating over the other players
			if (ActorHP && ActorHP->bIsDead) {
				continue;
			}

			//Actor isn't Dead 
			if (ActorAggro && ActorAggro->CurrentAggro > BiggestAggroInRange) {
				BiggestAggroInRange = ActorAggro->CurrentAggro;
				SetNewTarget(CurrentMinion);
			}
		}
	}
}

void ACPPSample_MobaTower_Tower::AttackTarget(float DeltaTime)
{
	CurrentAttackTimer = FMath::Clamp((CurrentAttackTimer + DeltaTime), 0.0f, TimeBetweenAttacks);

	if (CurrentAttackTimer >= TimeBetweenAttacks) {
		if (CurrentTarget) {
			FActorSpawnParameters args;
			args.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
			args.Owner = this;

			ACPPSample_MobaTower_TowerShot* spawnedShot = GetWorld()->SpawnActor<ACPPSample_MobaTower_TowerShot>(Projectile, ShooterMesh->GetComponentLocation(), FRotator(0.0f, 0.0f, 0.0f), args);
			spawnedShot->Target = CurrentTarget;
			spawnedShot->StartProjectile();
			CurrentAttackTimer = 0.0f;
		}
	}
}

// Called every frame
void ACPPSample_MobaTower_Tower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CheckIfTargetValid();
	DeterminateBestAggro();
	DrawAimLine();
	AttackTarget(DeltaTime);
}

void ACPPSample_MobaTower_Tower::HandleEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor == CurrentTarget) {
		ResetTarget();
	}
}

