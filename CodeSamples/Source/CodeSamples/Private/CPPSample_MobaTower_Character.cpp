// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPSample_MobaTower_Character.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "CPPSample_AggroComponent.h"
#include "CPPExample_HealthComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACPPSample_MobaTower_Character::ACPPSample_MobaTower_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bIsDead = false;

	//Setting my camera components and settings
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));
	CameraBoom->TargetArmLength = 1200;
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->bUsePawnControlRotation = false;
	CameraBoom->SetupAttachment(RootComponent);
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera Component"));
	CameraComponent->SetupAttachment(CameraBoom);

	// Setting up my Moba-related Components
	MyAggroComponent = CreateDefaultSubobject<UCPPSample_AggroComponent>(TEXT("Aggro Component"));
	MyHealthComponent = CreateDefaultSubobject<UCPPExample_HealthComponent>(TEXT("Health Component"));
	AddOwnedComponent(MyAggroComponent);
	AddOwnedComponent(MyHealthComponent);

	MyHealthComponent->OnDeathDelegate.AddDynamic(this, &ACPPSample_MobaTower_Character::OnDeathDelegate);
	OnTakeAnyDamage.AddDynamic(this, &ACPPSample_MobaTower_Character::TakenAnyDamage);
}

// Called when the game starts or when spawned
void ACPPSample_MobaTower_Character::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACPPSample_MobaTower_Character::MoveForward(float Scale)
{
	AddMovementInput(GetActorForwardVector(), Scale);
}

void ACPPSample_MobaTower_Character::MoveRight(float Scale)
{
	AddMovementInput(GetActorRightVector(), Scale);
}

void ACPPSample_MobaTower_Character::SimulateAttackingEnemy()
{
	MyAggroComponent->bIsPriority = true;
}

void ACPPSample_MobaTower_Character::StopSimulatingAttackingEnemy()
{
	MyAggroComponent->bIsPriority = false;
}

// Called every frame
void ACPPSample_MobaTower_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACPPSample_MobaTower_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ACPPSample_MobaTower_Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACPPSample_MobaTower_Character::MoveRight);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ACPPSample_MobaTower_Character::SimulateAttackingEnemy);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ACPPSample_MobaTower_Character::StopSimulatingAttackingEnemy);
}

void ACPPSample_MobaTower_Character::TakenAnyDamage(AActor * DamagedActor, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	MyHealthComponent->TakeDamage(Damage);
}

void ACPPSample_MobaTower_Character::OnDeathDelegate()
{
	APlayerController* MyController = UGameplayStatics::GetPlayerController(this, 0);
	GetMovementComponent()->StopMovementImmediately();
	DisableInput(MyController);
	bIsDead = true;
}

