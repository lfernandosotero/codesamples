// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_PickableComponent.h"
#include "GameFramework/Actor.h"
#include "Components/PrimitiveComponent.h"

// Sets default values for this component's properties
UCPP_PickableComponent::UCPP_PickableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCPP_PickableComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UCPP_PickableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UCPP_PickableComponent::SetupComponent(bool bCanBeGrabbed, bool bStartSimulatingPhysics)
{
	UCPP_PickableComponent::bCanBeGrabbed = bCanBeGrabbed;
	UCPP_PickableComponent::bStartSimulatingPhysics = bStartSimulatingPhysics;

	USceneComponent* myOwnerRoot = GetOwner()->GetRootComponent();
	if (Cast<UPrimitiveComponent>(myOwnerRoot)) {
		Cast<UPrimitiveComponent>(myOwnerRoot)->SetCollisionObjectType(ECC_WorldDynamic);
		Cast<UPrimitiveComponent>(myOwnerRoot)->SetSimulatePhysics(true);
		//Dropped down
	}
}

void UCPP_PickableComponent::DropMeDown()
{
	GetOwner()->SetActorEnableCollision(true);
	GetOwner()->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

	USceneComponent* myOwnerRoot = GetOwner()->GetRootComponent();
	if (Cast<UPrimitiveComponent>(myOwnerRoot)) {
		Cast<UPrimitiveComponent>(myOwnerRoot)->SetSimulatePhysics(true);
		//Dropped down
	}
}

