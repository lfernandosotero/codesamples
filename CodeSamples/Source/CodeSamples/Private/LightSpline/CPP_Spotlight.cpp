// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_Spotlight.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "CPP_PickableComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"

// Sets default values
ACPP_Spotlight::ACPP_Spotlight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bIsDebugOn = true;
	bIsAttached = true;
	bStartSimulatingPhysics = true;

	MyPickableComponent = CreateDefaultSubobject<UCPP_PickableComponent>(TEXT("Pickable Component"));
	AddOwnedComponent(MyPickableComponent);
}

// Called when the game starts or when spawned
void ACPP_Spotlight::BeginPlay()
{
	Super::BeginPlay();

	MyPickableComponent->SetupComponent(true,bStartSimulatingPhysics);
	APlayerController* MyController = UGameplayStatics::GetPlayerController(this, 0);
	if (MyController) {
		EnableInput(MyController);
	}
}

void ACPP_Spotlight::SetLookAtTarget()
{
	if (TargetActor) {
		LookAtTarget = TargetActor->GetActorLocation();
	}
	else {
		if (bTrackPlayerSight) {
			ACharacter* PlayerChar = UGameplayStatics::GetPlayerCharacter(this, 0);
			if (PlayerChar) {
				FHitResult HitResult;
				//GetWorld()->LineTraceSingleByChannel()
			}

		}
	}
}

void ACPP_Spotlight::RotateTowardsTarget(float DeltaTime)
{
	FVector TargetDirection = TargetActor->GetActorLocation() - GetActorLocation();
	TargetDirection.Normalize();

	FRotator NewLookAtRotation = FRotationMatrix::MakeFromX(TargetDirection).Rotator();
	SetActorRotation(FMath::RInterpTo(GetActorRotation(), NewLookAtRotation, DeltaTime, LookAtSpeed));
}

void ACPP_Spotlight::DrawDebugTargetLocation()
{
	if (bIsDebugOn) {
		DrawDebugSphere(GetWorld(), LookAtTarget, 20.0f, 12, FColor::Blue, false, -1.0f, (uint8)'\000', 2.0f);
		DrawDebugString(GetWorld(), LookAtTarget, TEXT("Sight Target"), (AActor *)0, FColor::White);
	}
}

// Called every frame
void ACPP_Spotlight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsAttached) {
	
	}
}

