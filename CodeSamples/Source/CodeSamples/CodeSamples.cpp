// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "CodeSamples.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CodeSamples, "CodeSamples" );
 