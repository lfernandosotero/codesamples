// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "CodeSamplesGameMode.h"
#include "CodeSamplesHUD.h"
#include "CodeSamplesCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACodeSamplesGameMode::ACodeSamplesGameMode()
	: Super()
{
	// use our custom HUD class
	//HUDClass = ACodeSamplesHUD::StaticClass();
}
