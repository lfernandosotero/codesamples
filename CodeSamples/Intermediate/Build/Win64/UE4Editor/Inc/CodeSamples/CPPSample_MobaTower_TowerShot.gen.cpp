// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/CPPMobaTower/CPPSample_MobaTower_TowerShot.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPPSample_MobaTower_TowerShot() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_TowerShot();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UProjectileMovementComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	void ACPPSample_MobaTower_TowerShot::StaticRegisterNativesACPPSample_MobaTower_TowerShot()
	{
	}
	UClass* Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_NoRegister()
	{
		return ACPPSample_MobaTower_TowerShot::StaticClass();
	}
	struct Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Damage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectileMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProjectileMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectileMovementComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProjectileMovementComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyRootComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyRootComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CPPMobaTower/CPPSample_MobaTower_TowerShot.h" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_TowerShot.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_Damage_MetaData[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_TowerShot.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_Damage = { UE4CodeGen_Private::EPropertyClass::Float, "Damage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_TowerShot, Damage), METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_Damage_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_Damage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMeshComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_TowerShot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMeshComponent = { UE4CodeGen_Private::EPropertyClass::Object, "ProjectileMeshComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_TowerShot, ProjectileMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMeshComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMovementComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_TowerShot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMovementComponent = { UE4CodeGen_Private::EPropertyClass::Object, "ProjectileMovementComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_TowerShot, ProjectileMovementComponent), Z_Construct_UClass_UProjectileMovementComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMovementComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMovementComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_MyRootComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_TowerShot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_MyRootComponent = { UE4CodeGen_Private::EPropertyClass::Object, "MyRootComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_TowerShot, MyRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_MyRootComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_MyRootComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_Damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_ProjectileMovementComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::NewProp_MyRootComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACPPSample_MobaTower_TowerShot>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::ClassParams = {
		&ACPPSample_MobaTower_TowerShot::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACPPSample_MobaTower_TowerShot()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACPPSample_MobaTower_TowerShot, 2299087607);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACPPSample_MobaTower_TowerShot(Z_Construct_UClass_ACPPSample_MobaTower_TowerShot, &ACPPSample_MobaTower_TowerShot::StaticClass, TEXT("/Script/CodeSamples"), TEXT("ACPPSample_MobaTower_TowerShot"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACPPSample_MobaTower_TowerShot);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
