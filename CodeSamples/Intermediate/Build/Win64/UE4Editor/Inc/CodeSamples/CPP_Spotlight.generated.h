// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CPP_Spotlight_generated_h
#error "CPP_Spotlight.generated.h already included, missing '#pragma once' in CPP_Spotlight.h"
#endif
#define CODESAMPLES_CPP_Spotlight_generated_h

#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPP_Spotlight(); \
	friend struct Z_Construct_UClass_ACPP_Spotlight_Statics; \
public: \
	DECLARE_CLASS(ACPP_Spotlight, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPP_Spotlight)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACPP_Spotlight(); \
	friend struct Z_Construct_UClass_ACPP_Spotlight_Statics; \
public: \
	DECLARE_CLASS(ACPP_Spotlight, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPP_Spotlight)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPP_Spotlight(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPP_Spotlight) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_Spotlight); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_Spotlight); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_Spotlight(ACPP_Spotlight&&); \
	NO_API ACPP_Spotlight(const ACPP_Spotlight&); \
public:


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_Spotlight(ACPP_Spotlight&&); \
	NO_API ACPP_Spotlight(const ACPP_Spotlight&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_Spotlight); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_Spotlight); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPP_Spotlight)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MyPickableComponent() { return STRUCT_OFFSET(ACPP_Spotlight, MyPickableComponent); } \
	FORCEINLINE static uint32 __PPO__SpotlightMesh() { return STRUCT_OFFSET(ACPP_Spotlight, SpotlightMesh); }


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_12_PROLOG
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_INCLASS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spotlight_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
