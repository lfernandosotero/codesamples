// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CPP_PickableComponent_generated_h
#error "CPP_PickableComponent.generated.h already included, missing '#pragma once' in CPP_PickableComponent.h"
#endif
#define CODESAMPLES_CPP_PickableComponent_generated_h

#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCPP_PickableComponent(); \
	friend struct Z_Construct_UClass_UCPP_PickableComponent_Statics; \
public: \
	DECLARE_CLASS(UCPP_PickableComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(UCPP_PickableComponent)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUCPP_PickableComponent(); \
	friend struct Z_Construct_UClass_UCPP_PickableComponent_Statics; \
public: \
	DECLARE_CLASS(UCPP_PickableComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(UCPP_PickableComponent)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCPP_PickableComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCPP_PickableComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCPP_PickableComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCPP_PickableComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCPP_PickableComponent(UCPP_PickableComponent&&); \
	NO_API UCPP_PickableComponent(const UCPP_PickableComponent&); \
public:


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCPP_PickableComponent(UCPP_PickableComponent&&); \
	NO_API UCPP_PickableComponent(const UCPP_PickableComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCPP_PickableComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCPP_PickableComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCPP_PickableComponent)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_PRIVATE_PROPERTY_OFFSET
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_11_PROLOG
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_INCLASS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_PickableComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
