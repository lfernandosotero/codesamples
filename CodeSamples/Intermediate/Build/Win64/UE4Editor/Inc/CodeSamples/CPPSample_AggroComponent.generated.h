// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CPPSample_AggroComponent_generated_h
#error "CPPSample_AggroComponent.generated.h already included, missing '#pragma once' in CPPSample_AggroComponent.h"
#endif
#define CODESAMPLES_CPPSample_AggroComponent_generated_h

#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCPPSample_AggroComponent(); \
	friend struct Z_Construct_UClass_UCPPSample_AggroComponent_Statics; \
public: \
	DECLARE_CLASS(UCPPSample_AggroComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(UCPPSample_AggroComponent)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUCPPSample_AggroComponent(); \
	friend struct Z_Construct_UClass_UCPPSample_AggroComponent_Statics; \
public: \
	DECLARE_CLASS(UCPPSample_AggroComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(UCPPSample_AggroComponent)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCPPSample_AggroComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCPPSample_AggroComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCPPSample_AggroComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCPPSample_AggroComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCPPSample_AggroComponent(UCPPSample_AggroComponent&&); \
	NO_API UCPPSample_AggroComponent(const UCPPSample_AggroComponent&); \
public:


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCPPSample_AggroComponent(UCPPSample_AggroComponent&&); \
	NO_API UCPPSample_AggroComponent(const UCPPSample_AggroComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCPPSample_AggroComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCPPSample_AggroComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCPPSample_AggroComponent)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_PRIVATE_PROPERTY_OFFSET
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_14_PROLOG
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_INCLASS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_AggroComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
