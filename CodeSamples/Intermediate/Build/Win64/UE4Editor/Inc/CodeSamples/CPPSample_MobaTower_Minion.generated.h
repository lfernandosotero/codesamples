// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UDamageType;
class AController;
#ifdef CODESAMPLES_CPPSample_MobaTower_Minion_generated_h
#error "CPPSample_MobaTower_Minion.generated.h already included, missing '#pragma once' in CPPSample_MobaTower_Minion.h"
#endif
#define CODESAMPLES_CPPSample_MobaTower_Minion_generated_h

#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnDeathDelegate) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnDeathDelegate(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTakenAnyDamage) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_DamagedActor); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Damage); \
		P_GET_OBJECT(UDamageType,Z_Param_DamageType); \
		P_GET_OBJECT(AController,Z_Param_InstigatedBy); \
		P_GET_OBJECT(AActor,Z_Param_DamageCauser); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TakenAnyDamage(Z_Param_DamagedActor,Z_Param_Damage,Z_Param_DamageType,Z_Param_InstigatedBy,Z_Param_DamageCauser); \
		P_NATIVE_END; \
	}


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnDeathDelegate) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnDeathDelegate(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTakenAnyDamage) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_DamagedActor); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Damage); \
		P_GET_OBJECT(UDamageType,Z_Param_DamageType); \
		P_GET_OBJECT(AController,Z_Param_InstigatedBy); \
		P_GET_OBJECT(AActor,Z_Param_DamageCauser); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TakenAnyDamage(Z_Param_DamagedActor,Z_Param_Damage,Z_Param_DamageType,Z_Param_InstigatedBy,Z_Param_DamageCauser); \
		P_NATIVE_END; \
	}


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPPSample_MobaTower_Minion(); \
	friend struct Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics; \
public: \
	DECLARE_CLASS(ACPPSample_MobaTower_Minion, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPPSample_MobaTower_Minion)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACPPSample_MobaTower_Minion(); \
	friend struct Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics; \
public: \
	DECLARE_CLASS(ACPPSample_MobaTower_Minion, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPPSample_MobaTower_Minion)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPPSample_MobaTower_Minion(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPPSample_MobaTower_Minion) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPSample_MobaTower_Minion); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPSample_MobaTower_Minion); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPSample_MobaTower_Minion(ACPPSample_MobaTower_Minion&&); \
	NO_API ACPPSample_MobaTower_Minion(const ACPPSample_MobaTower_Minion&); \
public:


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPSample_MobaTower_Minion(ACPPSample_MobaTower_Minion&&); \
	NO_API ACPPSample_MobaTower_Minion(const ACPPSample_MobaTower_Minion&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPSample_MobaTower_Minion); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPSample_MobaTower_Minion); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPPSample_MobaTower_Minion)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CylinderComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_Minion, CylinderComponent); } \
	FORCEINLINE static uint32 __PPO__MyAggroComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_Minion, MyAggroComponent); } \
	FORCEINLINE static uint32 __PPO__MyHealthComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_Minion, MyHealthComponent); }


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_12_PROLOG
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_INCLASS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Minion_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
