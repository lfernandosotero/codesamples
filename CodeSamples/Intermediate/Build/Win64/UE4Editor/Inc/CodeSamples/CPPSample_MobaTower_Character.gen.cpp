// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/CPPSample_MobaTower_Character.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPPSample_MobaTower_Character() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_Character_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_Character();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
	CODESAMPLES_API UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate();
	CODESAMPLES_API UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDamageType_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPPSample_AggroComponent_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPPExample_HealthComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	void ACPPSample_MobaTower_Character::StaticRegisterNativesACPPSample_MobaTower_Character()
	{
		UClass* Class = ACPPSample_MobaTower_Character::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnDeathDelegate", &ACPPSample_MobaTower_Character::execOnDeathDelegate },
			{ "TakenAnyDamage", &ACPPSample_MobaTower_Character::execTakenAnyDamage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CPPSample_MobaTower_Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACPPSample_MobaTower_Character, "OnDeathDelegate", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics
	{
		struct CPPSample_MobaTower_Character_eventTakenAnyDamage_Parms
		{
			AActor* DamagedActor;
			float Damage;
			const UDamageType* DamageType;
			AController* InstigatedBy;
			AActor* DamageCauser;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamageCauser;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InstigatedBy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DamageType_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamageType;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damage;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamagedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamageCauser = { UE4CodeGen_Private::EPropertyClass::Object, "DamageCauser", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Character_eventTakenAnyDamage_Parms, DamageCauser), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_InstigatedBy = { UE4CodeGen_Private::EPropertyClass::Object, "InstigatedBy", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Character_eventTakenAnyDamage_Parms, InstigatedBy), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamageType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamageType = { UE4CodeGen_Private::EPropertyClass::Object, "DamageType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Character_eventTakenAnyDamage_Parms, DamageType), Z_Construct_UClass_UDamageType_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamageType_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamageType_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_Damage = { UE4CodeGen_Private::EPropertyClass::Float, "Damage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Character_eventTakenAnyDamage_Parms, Damage), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamagedActor = { UE4CodeGen_Private::EPropertyClass::Object, "DamagedActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Character_eventTakenAnyDamage_Parms, DamagedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamageCauser,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_InstigatedBy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamageType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_Damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::NewProp_DamagedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CPPSample_MobaTower_Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACPPSample_MobaTower_Character, "TakenAnyDamage", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, sizeof(CPPSample_MobaTower_Character_eventTakenAnyDamage_Parms), Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACPPSample_MobaTower_Character_NoRegister()
	{
		return ACPPSample_MobaTower_Character::StaticClass();
	}
	struct Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyAggroComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyAggroComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyHealthComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyHealthComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACPPSample_MobaTower_Character_OnDeathDelegate, "OnDeathDelegate" }, // 2622255096
		{ &Z_Construct_UFunction_ACPPSample_MobaTower_Character_TakenAnyDamage, "TakenAnyDamage" }, // 1477101570
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "CPPSample_MobaTower_Character.h" },
		{ "ModuleRelativePath", "Public/CPPSample_MobaTower_Character.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyAggroComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPSample_MobaTower_Character.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyAggroComponent = { UE4CodeGen_Private::EPropertyClass::Object, "MyAggroComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Character, MyAggroComponent), Z_Construct_UClass_UCPPSample_AggroComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyAggroComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyAggroComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyHealthComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPSample_MobaTower_Character.h" },
		{ "ToolTip", "My Moba-Related Components" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyHealthComponent = { UE4CodeGen_Private::EPropertyClass::Object, "MyHealthComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Character, MyHealthComponent), Z_Construct_UClass_UCPPExample_HealthComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyHealthComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyHealthComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPSample_MobaTower_Character.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraBoom = { UE4CodeGen_Private::EPropertyClass::Object, "CameraBoom", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Character, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraBoom_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraComponent_MetaData[] = {
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPSample_MobaTower_Character.h" },
		{ "ToolTip", "Camera Components" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraComponent = { UE4CodeGen_Private::EPropertyClass::Object, "CameraComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Character, CameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyAggroComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_MyHealthComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraBoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::NewProp_CameraComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACPPSample_MobaTower_Character>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::ClassParams = {
		&ACPPSample_MobaTower_Character::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACPPSample_MobaTower_Character()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACPPSample_MobaTower_Character, 1388874608);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACPPSample_MobaTower_Character(Z_Construct_UClass_ACPPSample_MobaTower_Character, &ACPPSample_MobaTower_Character::StaticClass, TEXT("/Script/CodeSamples"), TEXT("ACPPSample_MobaTower_Character"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACPPSample_MobaTower_Character);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
