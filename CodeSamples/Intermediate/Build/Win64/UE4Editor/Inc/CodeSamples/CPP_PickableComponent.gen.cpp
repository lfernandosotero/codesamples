// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/LightSpline/CPP_PickableComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPP_PickableComponent() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPP_PickableComponent_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPP_PickableComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
// End Cross Module References
	void UCPP_PickableComponent::StaticRegisterNativesUCPP_PickableComponent()
	{
	}
	UClass* Z_Construct_UClass_UCPP_PickableComponent_NoRegister()
	{
		return UCPP_PickableComponent::StaticClass();
	}
	struct Z_Construct_UClass_UCPP_PickableComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCPP_PickableComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPP_PickableComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "LightSpline/CPP_PickableComponent.h" },
		{ "ModuleRelativePath", "Public/LightSpline/CPP_PickableComponent.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCPP_PickableComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCPP_PickableComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCPP_PickableComponent_Statics::ClassParams = {
		&UCPP_PickableComponent::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B000A4u,
		nullptr, 0,
		nullptr, 0,
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UCPP_PickableComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UCPP_PickableComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCPP_PickableComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCPP_PickableComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCPP_PickableComponent, 1845746011);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCPP_PickableComponent(Z_Construct_UClass_UCPP_PickableComponent, &UCPP_PickableComponent::StaticClass, TEXT("/Script/CodeSamples"), TEXT("UCPP_PickableComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCPP_PickableComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
