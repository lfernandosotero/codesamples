// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef CODESAMPLES_CPPSample_MobaTower_Tower_generated_h
#error "CPPSample_MobaTower_Tower.generated.h already included, missing '#pragma once' in CPPSample_MobaTower_Tower.h"
#endif
#define CODESAMPLES_CPPSample_MobaTower_Tower_generated_h

#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandleEndOverlap) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandleEndOverlap(Z_Param_OverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	}


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandleEndOverlap) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandleEndOverlap(Z_Param_OverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	}


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPPSample_MobaTower_Tower(); \
	friend struct Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics; \
public: \
	DECLARE_CLASS(ACPPSample_MobaTower_Tower, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPPSample_MobaTower_Tower)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_INCLASS \
private: \
	static void StaticRegisterNativesACPPSample_MobaTower_Tower(); \
	friend struct Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics; \
public: \
	DECLARE_CLASS(ACPPSample_MobaTower_Tower, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPPSample_MobaTower_Tower)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPPSample_MobaTower_Tower(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPPSample_MobaTower_Tower) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPSample_MobaTower_Tower); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPSample_MobaTower_Tower); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPSample_MobaTower_Tower(ACPPSample_MobaTower_Tower&&); \
	NO_API ACPPSample_MobaTower_Tower(const ACPPSample_MobaTower_Tower&); \
public:


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPSample_MobaTower_Tower(ACPPSample_MobaTower_Tower&&); \
	NO_API ACPPSample_MobaTower_Tower(const ACPPSample_MobaTower_Tower&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPSample_MobaTower_Tower); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPSample_MobaTower_Tower); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPPSample_MobaTower_Tower)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ShooterMesh() { return STRUCT_OFFSET(ACPPSample_MobaTower_Tower, ShooterMesh); } \
	FORCEINLINE static uint32 __PPO__RangeCollider() { return STRUCT_OFFSET(ACPPSample_MobaTower_Tower, RangeCollider); }


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_13_PROLOG
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_INCLASS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_Tower_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
