// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CodeSamplesGameMode_generated_h
#error "CodeSamplesGameMode.generated.h already included, missing '#pragma once' in CodeSamplesGameMode.h"
#endif
#define CODESAMPLES_CodeSamplesGameMode_generated_h

#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACodeSamplesGameMode(); \
	friend struct Z_Construct_UClass_ACodeSamplesGameMode_Statics; \
public: \
	DECLARE_CLASS(ACodeSamplesGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/CodeSamples"), CODESAMPLES_API) \
	DECLARE_SERIALIZER(ACodeSamplesGameMode)


#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACodeSamplesGameMode(); \
	friend struct Z_Construct_UClass_ACodeSamplesGameMode_Statics; \
public: \
	DECLARE_CLASS(ACodeSamplesGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/CodeSamples"), CODESAMPLES_API) \
	DECLARE_SERIALIZER(ACodeSamplesGameMode)


#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CODESAMPLES_API ACodeSamplesGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACodeSamplesGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CODESAMPLES_API, ACodeSamplesGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACodeSamplesGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CODESAMPLES_API ACodeSamplesGameMode(ACodeSamplesGameMode&&); \
	CODESAMPLES_API ACodeSamplesGameMode(const ACodeSamplesGameMode&); \
public:


#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CODESAMPLES_API ACodeSamplesGameMode(ACodeSamplesGameMode&&); \
	CODESAMPLES_API ACodeSamplesGameMode(const ACodeSamplesGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CODESAMPLES_API, ACodeSamplesGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACodeSamplesGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACodeSamplesGameMode)


#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_9_PROLOG
#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_INCLASS \
	CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_CodeSamplesGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
