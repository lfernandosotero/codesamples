// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/CPPMobaTower/CPPExample_HealthComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPPExample_HealthComponent() {}
// Cross Module References
	CODESAMPLES_API UFunction* Z_Construct_UDelegateFunction_CodeSamples_OnDeath__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
	CODESAMPLES_API UFunction* Z_Construct_UDelegateFunction_CodeSamples_OnDamageTaken__DelegateSignature();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPPExample_HealthComponent_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPPExample_HealthComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_CodeSamples_OnDeath__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CodeSamples_OnDeath__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPExample_HealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CodeSamples_OnDeath__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CodeSamples, "OnDeath__DelegateSignature", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00130000, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CodeSamples_OnDeath__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_CodeSamples_OnDeath__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CodeSamples_OnDeath__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CodeSamples_OnDeath__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_CodeSamples_OnDamageTaken__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CodeSamples_OnDamageTaken__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPExample_HealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CodeSamples_OnDamageTaken__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CodeSamples, "OnDamageTaken__DelegateSignature", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00130000, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CodeSamples_OnDamageTaken__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_CodeSamples_OnDamageTaken__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CodeSamples_OnDamageTaken__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CodeSamples_OnDamageTaken__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UCPPExample_HealthComponent::StaticRegisterNativesUCPPExample_HealthComponent()
	{
	}
	UClass* Z_Construct_UClass_UCPPExample_HealthComponent_NoRegister()
	{
		return UCPPExample_HealthComponent::StaticClass();
	}
	struct Z_Construct_UClass_UCPPExample_HealthComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsDead_MetaData[];
#endif
		static void NewProp_bIsDead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsDead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealthRegen_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HealthRegen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentHealth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCPPExample_HealthComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPExample_HealthComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "CPPMobaTower/CPPExample_HealthComponent.h" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPExample_HealthComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_bIsDead_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPExample_HealthComponent.h" },
	};
#endif
	void Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_bIsDead_SetBit(void* Obj)
	{
		((UCPPExample_HealthComponent*)Obj)->bIsDead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_bIsDead = { UE4CodeGen_Private::EPropertyClass::Bool, "bIsDead", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UCPPExample_HealthComponent), &Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_bIsDead_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_bIsDead_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_bIsDead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_HealthRegen_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPExample_HealthComponent.h" },
		{ "ToolTip", "Owner current health regen in Amount per second" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_HealthRegen = { UE4CodeGen_Private::EPropertyClass::Float, "HealthRegen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(UCPPExample_HealthComponent, HealthRegen), METADATA_PARAMS(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_HealthRegen_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_HealthRegen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_MaxHealth_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPExample_HealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_MaxHealth = { UE4CodeGen_Private::EPropertyClass::Float, "MaxHealth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(UCPPExample_HealthComponent, MaxHealth), METADATA_PARAMS(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_MaxHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_MaxHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_CurrentHealth_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPExample_HealthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_CurrentHealth = { UE4CodeGen_Private::EPropertyClass::Float, "CurrentHealth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010015, 1, nullptr, STRUCT_OFFSET(UCPPExample_HealthComponent, CurrentHealth), METADATA_PARAMS(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_CurrentHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_CurrentHealth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCPPExample_HealthComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_bIsDead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_HealthRegen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_MaxHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPExample_HealthComponent_Statics::NewProp_CurrentHealth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCPPExample_HealthComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCPPExample_HealthComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCPPExample_HealthComponent_Statics::ClassParams = {
		&UCPPExample_HealthComponent::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B000A4u,
		nullptr, 0,
		Z_Construct_UClass_UCPPExample_HealthComponent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UCPPExample_HealthComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCPPExample_HealthComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCPPExample_HealthComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCPPExample_HealthComponent, 2156544322);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCPPExample_HealthComponent(Z_Construct_UClass_UCPPExample_HealthComponent, &UCPPExample_HealthComponent::StaticClass, TEXT("/Script/CodeSamples"), TEXT("UCPPExample_HealthComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCPPExample_HealthComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
