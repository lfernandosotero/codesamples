// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CPPExample_HealthComponent_generated_h
#error "CPPExample_HealthComponent.generated.h already included, missing '#pragma once' in CPPExample_HealthComponent.h"
#endif
#define CODESAMPLES_CPPExample_HealthComponent_generated_h

#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_10_DELEGATE \
static inline void FOnDeath_DelegateWrapper(const FMulticastScriptDelegate& OnDeath) \
{ \
	OnDeath.ProcessMulticastDelegate<UObject>(NULL); \
}


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_9_DELEGATE \
static inline void FOnDamageTaken_DelegateWrapper(const FMulticastScriptDelegate& OnDamageTaken) \
{ \
	OnDamageTaken.ProcessMulticastDelegate<UObject>(NULL); \
}


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCPPExample_HealthComponent(); \
	friend struct Z_Construct_UClass_UCPPExample_HealthComponent_Statics; \
public: \
	DECLARE_CLASS(UCPPExample_HealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(UCPPExample_HealthComponent)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCPPExample_HealthComponent(); \
	friend struct Z_Construct_UClass_UCPPExample_HealthComponent_Statics; \
public: \
	DECLARE_CLASS(UCPPExample_HealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(UCPPExample_HealthComponent)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCPPExample_HealthComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCPPExample_HealthComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCPPExample_HealthComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCPPExample_HealthComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCPPExample_HealthComponent(UCPPExample_HealthComponent&&); \
	NO_API UCPPExample_HealthComponent(const UCPPExample_HealthComponent&); \
public:


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCPPExample_HealthComponent(UCPPExample_HealthComponent&&); \
	NO_API UCPPExample_HealthComponent(const UCPPExample_HealthComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCPPExample_HealthComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCPPExample_HealthComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCPPExample_HealthComponent)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_12_PROLOG
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_INCLASS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPExample_HealthComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
