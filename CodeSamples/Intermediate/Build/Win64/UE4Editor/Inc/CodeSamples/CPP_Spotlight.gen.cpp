// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/LightSpline/CPP_Spotlight.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPP_Spotlight() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPP_Spotlight_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPP_Spotlight();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPP_PickableComponent_NoRegister();
// End Cross Module References
	void ACPP_Spotlight::StaticRegisterNativesACPP_Spotlight()
	{
	}
	UClass* Z_Construct_UClass_ACPP_Spotlight_NoRegister()
	{
		return ACPP_Spotlight::StaticClass();
	}
	struct Z_Construct_UClass_ACPP_Spotlight_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpotlightMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpotlightMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyPickableComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyPickableComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACPP_Spotlight_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPP_Spotlight_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LightSpline/CPP_Spotlight.h" },
		{ "ModuleRelativePath", "Public/LightSpline/CPP_Spotlight.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_SpotlightMesh_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/LightSpline/CPP_Spotlight.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_SpotlightMesh = { UE4CodeGen_Private::EPropertyClass::Object, "SpotlightMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00200800000a000d, 1, nullptr, STRUCT_OFFSET(ACPP_Spotlight, SpotlightMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_SpotlightMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_SpotlightMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_MyPickableComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/LightSpline/CPP_Spotlight.h" },
		{ "ToolTip", "Components" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_MyPickableComponent = { UE4CodeGen_Private::EPropertyClass::Object, "MyPickableComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00200800000a000d, 1, nullptr, STRUCT_OFFSET(ACPP_Spotlight, MyPickableComponent), Z_Construct_UClass_UCPP_PickableComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_MyPickableComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_MyPickableComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACPP_Spotlight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_SpotlightMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPP_Spotlight_Statics::NewProp_MyPickableComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACPP_Spotlight_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACPP_Spotlight>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACPP_Spotlight_Statics::ClassParams = {
		&ACPP_Spotlight::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_ACPP_Spotlight_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACPP_Spotlight_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACPP_Spotlight_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACPP_Spotlight_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACPP_Spotlight()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACPP_Spotlight_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACPP_Spotlight, 1919671991);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACPP_Spotlight(Z_Construct_UClass_ACPP_Spotlight, &ACPP_Spotlight::StaticClass, TEXT("/Script/CodeSamples"), TEXT("ACPP_Spotlight"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACPP_Spotlight);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
