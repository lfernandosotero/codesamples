// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CPP_Spline_generated_h
#error "CPP_Spline.generated.h already included, missing '#pragma once' in CPP_Spline.h"
#endif
#define CODESAMPLES_CPP_Spline_generated_h

#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPP_Spline(); \
	friend struct Z_Construct_UClass_ACPP_Spline_Statics; \
public: \
	DECLARE_CLASS(ACPP_Spline, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPP_Spline)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACPP_Spline(); \
	friend struct Z_Construct_UClass_ACPP_Spline_Statics; \
public: \
	DECLARE_CLASS(ACPP_Spline, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPP_Spline)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPP_Spline(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPP_Spline) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_Spline); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_Spline); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_Spline(ACPP_Spline&&); \
	NO_API ACPP_Spline(const ACPP_Spline&); \
public:


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_Spline(ACPP_Spline&&); \
	NO_API ACPP_Spline(const ACPP_Spline&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_Spline); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_Spline); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPP_Spline)


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_PRIVATE_PROPERTY_OFFSET
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_9_PROLOG
#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_INCLASS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_LightSpline_CPP_Spline_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
