// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CodeSamplesCharacter_generated_h
#error "CodeSamplesCharacter.generated.h already included, missing '#pragma once' in CodeSamplesCharacter.h"
#endif
#define CODESAMPLES_CodeSamplesCharacter_generated_h

#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACodeSamplesCharacter(); \
	friend struct Z_Construct_UClass_ACodeSamplesCharacter_Statics; \
public: \
	DECLARE_CLASS(ACodeSamplesCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACodeSamplesCharacter)


#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACodeSamplesCharacter(); \
	friend struct Z_Construct_UClass_ACodeSamplesCharacter_Statics; \
public: \
	DECLARE_CLASS(ACodeSamplesCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACodeSamplesCharacter)


#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACodeSamplesCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACodeSamplesCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACodeSamplesCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACodeSamplesCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACodeSamplesCharacter(ACodeSamplesCharacter&&); \
	NO_API ACodeSamplesCharacter(const ACodeSamplesCharacter&); \
public:


#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACodeSamplesCharacter(ACodeSamplesCharacter&&); \
	NO_API ACodeSamplesCharacter(const ACodeSamplesCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACodeSamplesCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACodeSamplesCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACodeSamplesCharacter)


#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ACodeSamplesCharacter, FirstPersonCameraComponent); }


#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_11_PROLOG
#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_INCLASS \
	CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_CodeSamplesCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
