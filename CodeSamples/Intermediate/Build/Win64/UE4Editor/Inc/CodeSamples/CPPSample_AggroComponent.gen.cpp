// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/CPPMobaTower/CPPSample_AggroComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPPSample_AggroComponent() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPPSample_AggroComponent_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPPSample_AggroComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
// End Cross Module References
	void UCPPSample_AggroComponent::StaticRegisterNativesUCPPSample_AggroComponent()
	{
	}
	UClass* Z_Construct_UClass_UCPPSample_AggroComponent_NoRegister()
	{
		return UCPPSample_AggroComponent::StaticClass();
	}
	struct Z_Construct_UClass_UCPPSample_AggroComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HighAggroColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HighAggroColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowAggroColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LowAggroColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerBaseAggro_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlayerBaseAggro;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinionBaseAggro_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinionBaseAggro;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PriorityAggro_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PriorityAggro;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPriority_MetaData[];
#endif
		static void NewProp_bIsPriority_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPriority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AggroIncreaseByHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AggroIncreaseByHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentAggro_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentAggro;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCPPSample_AggroComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "CPPMobaTower/CPPSample_AggroComponent.h" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
		{ "ToolTip", "Aggro Component calculates an Aggro for a specific actor based on his status and health\n      Aggro is a value that informs how high priority this actor is for other actors to target or interact with\n      Actors generate Aggro when attacking enemy champions and have higher values when on low HP" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_HighAggroColor_MetaData[] = {
		{ "Category", "Default Aggro values" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_HighAggroColor = { UE4CodeGen_Private::EPropertyClass::Struct, "HighAggroColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(UCPPSample_AggroComponent, HighAggroColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_HighAggroColor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_HighAggroColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_LowAggroColor_MetaData[] = {
		{ "Category", "Default Aggro values" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
		{ "ToolTip", "Colors used for debugging" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_LowAggroColor = { UE4CodeGen_Private::EPropertyClass::Struct, "LowAggroColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(UCPPSample_AggroComponent, LowAggroColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_LowAggroColor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_LowAggroColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PlayerBaseAggro_MetaData[] = {
		{ "Category", "Default Aggro values" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PlayerBaseAggro = { UE4CodeGen_Private::EPropertyClass::Float, "PlayerBaseAggro", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(UCPPSample_AggroComponent, PlayerBaseAggro), METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PlayerBaseAggro_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PlayerBaseAggro_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_MinionBaseAggro_MetaData[] = {
		{ "Category", "Default Aggro values" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
		{ "ToolTip", "Base Aggro values for Players and Minions" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_MinionBaseAggro = { UE4CodeGen_Private::EPropertyClass::Float, "MinionBaseAggro", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(UCPPSample_AggroComponent, MinionBaseAggro), METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_MinionBaseAggro_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_MinionBaseAggro_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PriorityAggro_MetaData[] = {
		{ "Category", "Aggro" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PriorityAggro = { UE4CodeGen_Private::EPropertyClass::Float, "PriorityAggro", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(UCPPSample_AggroComponent, PriorityAggro), METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PriorityAggro_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PriorityAggro_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_bIsPriority_MetaData[] = {
		{ "Category", "Aggro" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
	};
#endif
	void Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_bIsPriority_SetBit(void* Obj)
	{
		((UCPPSample_AggroComponent*)Obj)->bIsPriority = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_bIsPriority = { UE4CodeGen_Private::EPropertyClass::Bool, "bIsPriority", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010015, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UCPPSample_AggroComponent), &Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_bIsPriority_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_bIsPriority_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_bIsPriority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_AggroIncreaseByHealth_MetaData[] = {
		{ "Category", "Aggro" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
		{ "ToolTip", "Ratio on how the aggro increases by missing health\n       Any value for Current Health between 0 and Max Health gives an Aggro in [0 - Value]\n       A value of 10 theoretically means that when the Current health / Max Health is 0 the actor gains 10 Aggro" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_AggroIncreaseByHealth = { UE4CodeGen_Private::EPropertyClass::Float, "AggroIncreaseByHealth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(UCPPSample_AggroComponent, AggroIncreaseByHealth), METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_AggroIncreaseByHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_AggroIncreaseByHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_CurrentAggro_MetaData[] = {
		{ "Category", "Aggro" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_AggroComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_CurrentAggro = { UE4CodeGen_Private::EPropertyClass::Float, "CurrentAggro", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010015, 1, nullptr, STRUCT_OFFSET(UCPPSample_AggroComponent, CurrentAggro), METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_CurrentAggro_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_CurrentAggro_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCPPSample_AggroComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_HighAggroColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_LowAggroColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PlayerBaseAggro,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_MinionBaseAggro,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_PriorityAggro,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_bIsPriority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_AggroIncreaseByHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCPPSample_AggroComponent_Statics::NewProp_CurrentAggro,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCPPSample_AggroComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCPPSample_AggroComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCPPSample_AggroComponent_Statics::ClassParams = {
		&UCPPSample_AggroComponent::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B000A4u,
		nullptr, 0,
		Z_Construct_UClass_UCPPSample_AggroComponent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UCPPSample_AggroComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCPPSample_AggroComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCPPSample_AggroComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCPPSample_AggroComponent, 2940781912);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCPPSample_AggroComponent(Z_Construct_UClass_UCPPSample_AggroComponent, &UCPPSample_AggroComponent::StaticClass, TEXT("/Script/CodeSamples"), TEXT("UCPPSample_AggroComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCPPSample_AggroComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
