// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/CPPMobaTower/CPPSample_MobaTower_Minion.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPPSample_MobaTower_Minion() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_Minion_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_Minion();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
	CODESAMPLES_API UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate();
	CODESAMPLES_API UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDamageType_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPPExample_HealthComponent_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_UCPPSample_AggroComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void ACPPSample_MobaTower_Minion::StaticRegisterNativesACPPSample_MobaTower_Minion()
	{
		UClass* Class = ACPPSample_MobaTower_Minion::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnDeathDelegate", &ACPPSample_MobaTower_Minion::execOnDeathDelegate },
			{ "TakenAnyDamage", &ACPPSample_MobaTower_Minion::execTakenAnyDamage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Minion.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACPPSample_MobaTower_Minion, "OnDeathDelegate", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics
	{
		struct CPPSample_MobaTower_Minion_eventTakenAnyDamage_Parms
		{
			AActor* DamagedActor;
			float Damage;
			const UDamageType* DamageType;
			AController* InstigatedBy;
			AActor* DamageCauser;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamageCauser;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InstigatedBy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DamageType_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamageType;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damage;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamagedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamageCauser = { UE4CodeGen_Private::EPropertyClass::Object, "DamageCauser", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Minion_eventTakenAnyDamage_Parms, DamageCauser), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_InstigatedBy = { UE4CodeGen_Private::EPropertyClass::Object, "InstigatedBy", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Minion_eventTakenAnyDamage_Parms, InstigatedBy), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamageType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamageType = { UE4CodeGen_Private::EPropertyClass::Object, "DamageType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Minion_eventTakenAnyDamage_Parms, DamageType), Z_Construct_UClass_UDamageType_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamageType_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamageType_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_Damage = { UE4CodeGen_Private::EPropertyClass::Float, "Damage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Minion_eventTakenAnyDamage_Parms, Damage), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamagedActor = { UE4CodeGen_Private::EPropertyClass::Object, "DamagedActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Minion_eventTakenAnyDamage_Parms, DamagedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamageCauser,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_InstigatedBy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamageType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_Damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::NewProp_DamagedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Minion.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACPPSample_MobaTower_Minion, "TakenAnyDamage", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, sizeof(CPPSample_MobaTower_Minion_eventTakenAnyDamage_Parms), Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACPPSample_MobaTower_Minion_NoRegister()
	{
		return ACPPSample_MobaTower_Minion::StaticClass();
	}
	struct Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyHealthComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyHealthComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyAggroComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyAggroComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CylinderComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CylinderComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACPPSample_MobaTower_Minion_OnDeathDelegate, "OnDeathDelegate" }, // 2662124635
		{ &Z_Construct_UFunction_ACPPSample_MobaTower_Minion_TakenAnyDamage, "TakenAnyDamage" }, // 738322552
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "CPPMobaTower/CPPSample_MobaTower_Minion.h" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Minion.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyHealthComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Minion.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyHealthComponent = { UE4CodeGen_Private::EPropertyClass::Object, "MyHealthComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Minion, MyHealthComponent), Z_Construct_UClass_UCPPExample_HealthComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyHealthComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyHealthComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyAggroComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Minion.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyAggroComponent = { UE4CodeGen_Private::EPropertyClass::Object, "MyAggroComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Minion, MyAggroComponent), Z_Construct_UClass_UCPPSample_AggroComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyAggroComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyAggroComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_CylinderComponent_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Minion.h" },
		{ "ToolTip", "My Mesh representation" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_CylinderComponent = { UE4CodeGen_Private::EPropertyClass::Object, "CylinderComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Minion, CylinderComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_CylinderComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_CylinderComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyHealthComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_MyAggroComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::NewProp_CylinderComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACPPSample_MobaTower_Minion>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::ClassParams = {
		&ACPPSample_MobaTower_Minion::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACPPSample_MobaTower_Minion()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACPPSample_MobaTower_Minion_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACPPSample_MobaTower_Minion, 4166752185);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACPPSample_MobaTower_Minion(Z_Construct_UClass_ACPPSample_MobaTower_Minion, &ACPPSample_MobaTower_Minion::StaticClass, TEXT("/Script/CodeSamples"), TEXT("ACPPSample_MobaTower_Minion"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACPPSample_MobaTower_Minion);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
