// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/CodeSamplesHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCodeSamplesHUD() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_ACodeSamplesHUD_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_ACodeSamplesHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
// End Cross Module References
	void ACodeSamplesHUD::StaticRegisterNativesACodeSamplesHUD()
	{
	}
	UClass* Z_Construct_UClass_ACodeSamplesHUD_NoRegister()
	{
		return ACodeSamplesHUD::StaticClass();
	}
	struct Z_Construct_UClass_ACodeSamplesHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACodeSamplesHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACodeSamplesHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "CodeSamplesHUD.h" },
		{ "ModuleRelativePath", "CodeSamplesHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACodeSamplesHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACodeSamplesHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACodeSamplesHUD_Statics::ClassParams = {
		&ACodeSamplesHUD::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008002ACu,
		nullptr, 0,
		nullptr, 0,
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACodeSamplesHUD_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACodeSamplesHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACodeSamplesHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACodeSamplesHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACodeSamplesHUD, 1346545160);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACodeSamplesHUD(Z_Construct_UClass_ACodeSamplesHUD, &ACodeSamplesHUD::StaticClass, TEXT("/Script/CodeSamples"), TEXT("ACodeSamplesHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACodeSamplesHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
