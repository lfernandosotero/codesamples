// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CodeSamplesHUD_generated_h
#error "CodeSamplesHUD.generated.h already included, missing '#pragma once' in CodeSamplesHUD.h"
#endif
#define CODESAMPLES_CodeSamplesHUD_generated_h

#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACodeSamplesHUD(); \
	friend struct Z_Construct_UClass_ACodeSamplesHUD_Statics; \
public: \
	DECLARE_CLASS(ACodeSamplesHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACodeSamplesHUD)


#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACodeSamplesHUD(); \
	friend struct Z_Construct_UClass_ACodeSamplesHUD_Statics; \
public: \
	DECLARE_CLASS(ACodeSamplesHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACodeSamplesHUD)


#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACodeSamplesHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACodeSamplesHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACodeSamplesHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACodeSamplesHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACodeSamplesHUD(ACodeSamplesHUD&&); \
	NO_API ACodeSamplesHUD(const ACodeSamplesHUD&); \
public:


#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACodeSamplesHUD(ACodeSamplesHUD&&); \
	NO_API ACodeSamplesHUD(const ACodeSamplesHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACodeSamplesHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACodeSamplesHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACodeSamplesHUD)


#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_9_PROLOG
#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_INCLASS \
	CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_CodeSamplesHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_CodeSamplesHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
