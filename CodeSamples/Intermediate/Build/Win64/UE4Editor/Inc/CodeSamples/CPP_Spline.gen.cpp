// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/LightSpline/CPP_Spline.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPP_Spline() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPP_Spline_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPP_Spline();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
// End Cross Module References
	void ACPP_Spline::StaticRegisterNativesACPP_Spline()
	{
	}
	UClass* Z_Construct_UClass_ACPP_Spline_NoRegister()
	{
		return ACPP_Spline::StaticClass();
	}
	struct Z_Construct_UClass_ACPP_Spline_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACPP_Spline_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPP_Spline_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LightSpline/CPP_Spline.h" },
		{ "ModuleRelativePath", "Public/LightSpline/CPP_Spline.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACPP_Spline_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACPP_Spline>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACPP_Spline_Statics::ClassParams = {
		&ACPP_Spline::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACPP_Spline_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACPP_Spline_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACPP_Spline()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACPP_Spline_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACPP_Spline, 1523157392);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACPP_Spline(Z_Construct_UClass_ACPP_Spline, &ACPP_Spline::StaticClass, TEXT("/Script/CodeSamples"), TEXT("ACPP_Spline"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACPP_Spline);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
