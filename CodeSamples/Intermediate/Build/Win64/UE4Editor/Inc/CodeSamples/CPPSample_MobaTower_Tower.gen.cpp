// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeSamples/Public/CPPMobaTower/CPPSample_MobaTower_Tower.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPPSample_MobaTower_Tower() {}
// Cross Module References
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_Tower_NoRegister();
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_Tower();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_CodeSamples();
	CODESAMPLES_API UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USphereComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	CODESAMPLES_API UClass* Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_NoRegister();
// End Cross Module References
	void ACPPSample_MobaTower_Tower::StaticRegisterNativesACPPSample_MobaTower_Tower()
	{
		UClass* Class = ACPPSample_MobaTower_Tower::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "HandleEndOverlap", &ACPPSample_MobaTower_Tower::execHandleEndOverlap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics
	{
		struct CPPSample_MobaTower_Tower_eventHandleEndOverlap_Parms
		{
			AActor* OverlappedActor;
			AActor* OtherActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::NewProp_OtherActor = { UE4CodeGen_Private::EPropertyClass::Object, "OtherActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Tower_eventHandleEndOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::NewProp_OverlappedActor = { UE4CodeGen_Private::EPropertyClass::Object, "OverlappedActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CPPSample_MobaTower_Tower_eventHandleEndOverlap_Parms, OverlappedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::NewProp_OverlappedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Tower.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACPPSample_MobaTower_Tower, "HandleEndOverlap", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, sizeof(CPPSample_MobaTower_Tower_eventHandleEndOverlap_Parms), Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACPPSample_MobaTower_Tower_NoRegister()
	{
		return ACPPSample_MobaTower_Tower::StaticClass();
	}
	struct Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RangeCollider_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RangeCollider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShooterMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShooterMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Projectile_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Projectile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeBetweenAttacks_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeBetweenAttacks;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeSamples,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACPPSample_MobaTower_Tower_HandleEndOverlap, "HandleEndOverlap" }, // 704651997
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CPPMobaTower/CPPSample_MobaTower_Tower.h" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Tower.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_RangeCollider_MetaData[] = {
		{ "Category", "Shooting" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Tower.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_RangeCollider = { UE4CodeGen_Private::EPropertyClass::Object, "RangeCollider", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Tower, RangeCollider), Z_Construct_UClass_USphereComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_RangeCollider_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_RangeCollider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_ShooterMesh_MetaData[] = {
		{ "Category", "Shooting" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Tower.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_ShooterMesh = { UE4CodeGen_Private::EPropertyClass::Object, "ShooterMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000009000d, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Tower, ShooterMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_ShooterMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_ShooterMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_Projectile_MetaData[] = {
		{ "Category", "Shooting" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Tower.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_Projectile = { UE4CodeGen_Private::EPropertyClass::Class, "Projectile", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000010005, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Tower, Projectile), Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_Projectile_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_Projectile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_TimeBetweenAttacks_MetaData[] = {
		{ "Category", "Shooting" },
		{ "ModuleRelativePath", "Public/CPPMobaTower/CPPSample_MobaTower_Tower.h" },
		{ "ToolTip", "Time in seconds between tower shots\n               tower can only shoot when it has spent\n               TimeBetweenAttacks seconds without shooting" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_TimeBetweenAttacks = { UE4CodeGen_Private::EPropertyClass::Float, "TimeBetweenAttacks", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ACPPSample_MobaTower_Tower, TimeBetweenAttacks), METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_TimeBetweenAttacks_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_TimeBetweenAttacks_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_RangeCollider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_ShooterMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_Projectile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::NewProp_TimeBetweenAttacks,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACPPSample_MobaTower_Tower>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::ClassParams = {
		&ACPPSample_MobaTower_Tower::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACPPSample_MobaTower_Tower()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACPPSample_MobaTower_Tower_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACPPSample_MobaTower_Tower, 1226248285);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACPPSample_MobaTower_Tower(Z_Construct_UClass_ACPPSample_MobaTower_Tower, &ACPPSample_MobaTower_Tower::StaticClass, TEXT("/Script/CodeSamples"), TEXT("ACPPSample_MobaTower_Tower"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACPPSample_MobaTower_Tower);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
