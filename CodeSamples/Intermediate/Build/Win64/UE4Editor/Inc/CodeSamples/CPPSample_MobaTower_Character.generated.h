// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UDamageType;
class AController;
#ifdef CODESAMPLES_CPPSample_MobaTower_Character_generated_h
#error "CPPSample_MobaTower_Character.generated.h already included, missing '#pragma once' in CPPSample_MobaTower_Character.h"
#endif
#define CODESAMPLES_CPPSample_MobaTower_Character_generated_h

#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnDeathDelegate) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnDeathDelegate(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTakenAnyDamage) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_DamagedActor); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Damage); \
		P_GET_OBJECT(UDamageType,Z_Param_DamageType); \
		P_GET_OBJECT(AController,Z_Param_InstigatedBy); \
		P_GET_OBJECT(AActor,Z_Param_DamageCauser); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TakenAnyDamage(Z_Param_DamagedActor,Z_Param_Damage,Z_Param_DamageType,Z_Param_InstigatedBy,Z_Param_DamageCauser); \
		P_NATIVE_END; \
	}


#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnDeathDelegate) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnDeathDelegate(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTakenAnyDamage) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_DamagedActor); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Damage); \
		P_GET_OBJECT(UDamageType,Z_Param_DamageType); \
		P_GET_OBJECT(AController,Z_Param_InstigatedBy); \
		P_GET_OBJECT(AActor,Z_Param_DamageCauser); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TakenAnyDamage(Z_Param_DamagedActor,Z_Param_Damage,Z_Param_DamageType,Z_Param_InstigatedBy,Z_Param_DamageCauser); \
		P_NATIVE_END; \
	}


#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPPSample_MobaTower_Character(); \
	friend struct Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics; \
public: \
	DECLARE_CLASS(ACPPSample_MobaTower_Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPPSample_MobaTower_Character)


#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_INCLASS \
private: \
	static void StaticRegisterNativesACPPSample_MobaTower_Character(); \
	friend struct Z_Construct_UClass_ACPPSample_MobaTower_Character_Statics; \
public: \
	DECLARE_CLASS(ACPPSample_MobaTower_Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPPSample_MobaTower_Character)


#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPPSample_MobaTower_Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPPSample_MobaTower_Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPSample_MobaTower_Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPSample_MobaTower_Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPSample_MobaTower_Character(ACPPSample_MobaTower_Character&&); \
	NO_API ACPPSample_MobaTower_Character(const ACPPSample_MobaTower_Character&); \
public:


#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPSample_MobaTower_Character(ACPPSample_MobaTower_Character&&); \
	NO_API ACPPSample_MobaTower_Character(const ACPPSample_MobaTower_Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPSample_MobaTower_Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPSample_MobaTower_Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPPSample_MobaTower_Character)


#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_Character, CameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ACPPSample_MobaTower_Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__MyHealthComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_Character, MyHealthComponent); } \
	FORCEINLINE static uint32 __PPO__MyAggroComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_Character, MyAggroComponent); }


#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_15_PROLOG
#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_INCLASS \
	CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_CPPSample_MobaTower_Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
