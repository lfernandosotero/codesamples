// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODESAMPLES_CPPSample_MobaTower_TowerShot_generated_h
#error "CPPSample_MobaTower_TowerShot.generated.h already included, missing '#pragma once' in CPPSample_MobaTower_TowerShot.h"
#endif
#define CODESAMPLES_CPPSample_MobaTower_TowerShot_generated_h

#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_RPC_WRAPPERS
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPPSample_MobaTower_TowerShot(); \
	friend struct Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics; \
public: \
	DECLARE_CLASS(ACPPSample_MobaTower_TowerShot, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPPSample_MobaTower_TowerShot)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_INCLASS \
private: \
	static void StaticRegisterNativesACPPSample_MobaTower_TowerShot(); \
	friend struct Z_Construct_UClass_ACPPSample_MobaTower_TowerShot_Statics; \
public: \
	DECLARE_CLASS(ACPPSample_MobaTower_TowerShot, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeSamples"), NO_API) \
	DECLARE_SERIALIZER(ACPPSample_MobaTower_TowerShot)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPPSample_MobaTower_TowerShot(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPPSample_MobaTower_TowerShot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPSample_MobaTower_TowerShot); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPSample_MobaTower_TowerShot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPSample_MobaTower_TowerShot(ACPPSample_MobaTower_TowerShot&&); \
	NO_API ACPPSample_MobaTower_TowerShot(const ACPPSample_MobaTower_TowerShot&); \
public:


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPSample_MobaTower_TowerShot(ACPPSample_MobaTower_TowerShot&&); \
	NO_API ACPPSample_MobaTower_TowerShot(const ACPPSample_MobaTower_TowerShot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPSample_MobaTower_TowerShot); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPSample_MobaTower_TowerShot); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPPSample_MobaTower_TowerShot)


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MyRootComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_TowerShot, MyRootComponent); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovementComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_TowerShot, ProjectileMovementComponent); } \
	FORCEINLINE static uint32 __PPO__ProjectileMeshComponent() { return STRUCT_OFFSET(ACPPSample_MobaTower_TowerShot, ProjectileMeshComponent); } \
	FORCEINLINE static uint32 __PPO__Damage() { return STRUCT_OFFSET(ACPPSample_MobaTower_TowerShot, Damage); }


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_13_PROLOG
#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_RPC_WRAPPERS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_INCLASS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_PRIVATE_PROPERTY_OFFSET \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_INCLASS_NO_PURE_DECLS \
	CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodeSamples_Source_CodeSamples_Public_CPPMobaTower_CPPSample_MobaTower_TowerShot_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
